$(function(){
    initProgrammsMenu();
    initScroll();
    initPhotosMenu();
    ajaxLoadNextPhotos();
    initPopup();
    example_SubmitForm();

    initPhotoSlider();
    initImagePopup();
    setInterval(function() { reviewsSlider(); }, 12000);
});

function initProgrammsMenu() {
    $(".programms_cnt li").click(function(){
        $(".programms_cnt li").removeClass("active");
        $(this).addClass("active");
        $("#programms_content li").removeClass("active").removeClass("animate");
        $($("#programms_content li.programm_item")[$(this).index()]).addClass("active").addClass("animate");
        scrtollAnimate($(this).children("a"));
    });
}

function initPhotosMenu() {
    $(".select_list li").click(function(){
        $(".select_list li").removeClass("active");
        $(this).addClass("active");
        $(".photo_list .photo_items").removeClass("active").removeClass("animate");
        $($(".photo_list .photo_items")[$(this).index()]).addClass("active").addClass("animate");
        return false;
    });
}

function ajaxLoadNextPhotos() {
    $(".photo_list .btn").click(function(){
        i = 0;
        while (i < 4) {
            if (i < 2)
                $(this).parent().parent().children("ul.content").append('<li class="animate left"><div class="arrow"></div><img src="images/employee/photo%201.jpg"><div class="text"><div class="title">Title Name Titled</div></div></li>');
            else
                $(this).parent().parent().children("ul.content").append('<li class="animate right"><div class="arrow"></div><div class="text"><div class="title">Title Name Titled</div></div><img src="images/employee/photo%201.jpg"></li>');
            i = i + 1
        }
    });
}

function initScroll() {
    $("a.scroll").click(function() {
        scrtollAnimate(this);
        return false;
    });
}

function scrtollAnimate(object) {
    $("html, body").animate({
        scrollTop: $($(object).attr("href")).offset().top + "px"
    }, {
        duration: 500
    });
}

function initPopup() {
    $("#ask_question_link").click(function(){
        $(".popup.ask_popup").show();
    });
    $("#send_review").click(function(){
        $(".popup.review_popup").show();
    });
    $("body").on("click", ".popup", function(){
        $(".popup").hide();
    });
    $(".popup .form").click(function(e) {
        e.stopPropagation();
    });
}

function initImagePopup() {
    $("#photo_slider_cnt img").click(function(){
        image_src = $(this).parent().data("img");
        $(".image_popup img").attr("src", image_src);
        $(".image_popup").show();
    });
}

function initPhotoSlider() {

    object = $("#photo_slider_cnt");
    children = object.children(".slider_children");
    $(children).children("li").first().addClass("current");
    reinitChildren();

    $($(object).children(".left_switcher")).click(function(){
        previousSlide();
    });

    $($(object).children(".right_switcher")).click(function(){
        nextSlide();
    });

    function nextSlide() {
        slideAnimation("right");
    }

    function previousSlide() {
        slideAnimation("left");
    }

    function slideAnimation(type) {
        if ($("body").hasClass("mobile")) {
            admixture = 0;
        } else {
            admixture = 2;
        }

        current = $($(children).children("li.current")).index();
        length = $(children).children("li").length - 1;
        width = $(children).children("li.current").width() + 20;
        if (length > 2) {
            switch (type) {
                case "right":
                    if (current == length - admixture)
                        current = 0;
                    else
                        current = current + 1;
                    break;
                case "left":
                    if (current == 0)
                        current = length - admixture;
                    else
                        current = current - 1;
                    break;
            }
            $(children).animate({left: -( current * width )}, 300);
            $($(children).children("li")).removeClass("current");
            $($(children).children("li")[current]).addClass("current");
        }
    }

    function reinitChildren() {
        if ($("body").hasClass("mobile")) {
            width = $(children).children("li.current").width() + 20;
            current = $($(children).children("li.current")).index();
            $(children).children("li").width($(window).width() - 158);
            $(children).css("left", -( current * width ));
        }
    }

    $(window).resize(function(){
        reinitChildren();
    });
}


function example_SubmitForm() {
    $("form").submit(function(){
        alert("Не забудь убрать вызов этой функции для успешной работы формы. Это пример работы с сообщениями об успешной отправки форм");
        $(this).children("input, textarea, .captcha").hide();
        $(this).children(".info").show();
        return false;
    });
}

function reviewsSlider() {
    current = $(".reviews_items .review.active");
    current.removeClass("active").removeClass("animate");
    if (current.next().length != 0)
        obj = current.next();
    else
        obj = $(".reviews_items .review").first();
    obj.addClass("active").addClass("animate");
}